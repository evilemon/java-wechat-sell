package com.imooc.util;

import java.util.Random;

public class KeyUtil {
    // 时间 + 六位随机数
    public static synchronized String genUniqueKey() {
        Random random = new Random();
        Integer number = random.nextInt(900000) + 100000;
        return System.currentTimeMillis() + String.valueOf(number);
    }
}
