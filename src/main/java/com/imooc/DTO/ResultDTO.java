package com.imooc.DTO;

import lombok.Data;

/**
 * 数据转化整合
 */

@Data
public class ResultDTO<T> {
    /**
     * 错误码
     */
    private Integer code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 具体内容
     */
    private T data;
}
