package com.imooc.DTO;


import com.imooc.dataobject.OrderDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class OrderDTO {

    // 订单编号
    private String orderId;

    // 买家姓名
    private String buyerName;

    // 卖家电话
    private String buyerPhone;

    // 买家地址
    private String buyerAddress;

    // 买家微信
    private String buyerOpenid;

    // 订单总金额
    private BigDecimal orderAmount;

    // 订单状态
    private Integer orderStatus;

    // 支付状态
    private Integer payStatus;

    // 订单创建时间
    private Date createTime;

    // 订单更新时间
    private Date updateTime;

    private List<OrderDetail> orderDetailList;

}
