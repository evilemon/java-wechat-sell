package com.imooc.enums;

import lombok.Getter;

@Getter
public enum PayStatusEnum {
    WAIT(0, "等待"),
    SUCCESS(1, "成功")
    ;
    private Integer code;
    private String message;

    PayStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
